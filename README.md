>!['FrozenFist' courtesy of Imgur](http://i.imgur.com/R2EVYxS.png)

# Project FrozenFist

## VortexOps

>This is project codename "Frozen Fist." Our goal is to design software for semi-autonomous armored assault vehicles.  
These vehicles will be able to operate remotely via a human pilot, or autonomously via guidance from an on-board AI system.  
My job is to help the team with planning, organization, and designing the application for the project.  
In short, I'm doing everything that doesn't involve writing code in order to free up the time for the other high-salaried software devs and to save costs by having me do it!  
Through this project, I'm able to see first-hand how software design and project development works.

Contact: [Allen Zayden](<mailto:allen.zayden@smail.rasmussen.edu>)

### Pushed to BitBucket repository - 02/18/2018

### Modified README on BitBucket - 02/25/2018